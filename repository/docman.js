module.exports = {
    sections: {
        docmanHomepage: {
            selector: 'body',
            elements: {
                admin: {locateStrategy: 'xpath', selector: "//a[contains(.,'Administration')]"},
                data_management: {locateStrategy: 'xpath', selector: "//a[contains(.,'Document Management')]"},
                save_link: {locateStrategy: 'xpath', selector: "//a[contains(.,'Save To DocMan')]"},
                next: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[2]"},
                line_bussiness: {locateStrategy: 'xpath', selector: "(//div[@id='InitialIndexingView']//tr//td//select)[1]"},
                select_type: {locateStrategy: 'xpath', selector: "//option[@value='DB']"},
                document_type: {locateStrategy: 'xpath', selector: "(//div[@id='InitialIndexingView']//tr//td//select)[2]"},
                beneficiary: {locateStrategy: 'xpath', selector: "//option[@value='BENE']"},
                submit: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[5]"},
                search: {locateStrategy: 'xpath', selector: "//input[@type='button']"},
                client_code: {locateStrategy: 'xpath', selector: "(//input[@type='text'])[1]"},
                submit2: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[1]"},
                select_test: {locateStrategy: 'xpath', selector: "(//tr[@class='normalrow']//td)[1]"},
                ssn: {locateStrategy: 'xpath', selector: " (//td//input[@type='text'])[6]"},
                add: {locateStrategy: 'xpath', selector: "(//div[@class='buttonized']//input[@type='submit'])[1]"},
                main_submit: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[11]"},


                next_action: {locateStrategy: 'xpath', selector: "(//td[@class='form_data']//select)[2]"},
                Send_for_processing: {locateStrategy: 'xpath', selector: "//option[contains(.,'Send For Processing')]"},
                search_select: {locateStrategy: 'xpath', selector: "//div[@id='FooterView']//input[@value='Search']"},
                last_name_select: {locateStrategy: 'xpath', selector: "(//input[@type='text'])[1]"},
                kuna_search_select: {locateStrategy: 'xpath', selector: "//input[@type='button']"},
                Select_name: {locateStrategy: 'xpath', selector: "(//td[contains(.,'Sneha')])[5]"},
                Select_role: {locateStrategy: 'xpath', selector: "(//select)[2]"},
                Select_role_qa: {locateStrategy: 'xpath', selector: "//option[contains(.,'BPMSRole.QA.Analyst.Document Management')]"},
                assign: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[3]"},
                trace_no: {locateStrategy: 'xpath', selector: "//td//span//b"},
                //////////////////2//////

                audit_report: {locateStrategy: 'xpath', selector: " //a[contains(.,'Audit Report')]"},
                from_date: {locateStrategy: 'xpath', selector: "(//input[@type='text'])[1]"},
                report_type: {locateStrategy: 'xpath', selector: "//tr//td//select"},
                all_counts: {locateStrategy: 'xpath', selector: "(//tr//td//select//option)[1]"},
                view_summary: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[1]"},
                captured: {locateStrategy: 'xpath', selector: "//tr//td//span[@id='ctl00_cpMain_lblTotalCaptured']"},
                detail_report: {locateStrategy: 'xpath', selector: "(//input[@type='submit'])[2]"},
                back: {locateStrategy: 'xpath', selector: "(//input[@type='image'])[1]"},
                save_to: {locateStrategy: 'xpath', selector: "(//tr//td//select//option)[7]"},

                /////////////3/////////////////

                Health_Check: {locateStrategy: 'xpath', selector: "//a[contains(.,'Health Check')]"},
                success: {locateStrategy: 'xpath', selector: "(//span[contains(.,'SUCCESS')])[1]"},

                ////////////4//////////

                search_view: {locateStrategy: 'xpath', selector: "//a[contains(.,'Search And\n" +
                    "                View')]"},
                track: {locateStrategy: 'xpath', selector: "(//input[@class='form_data'])[1]"},
                test_no_present: {locateStrategy: 'xpath', selector: "(//tr[@class='DocManSelectableGridSelected']//td)[4]"},
                search_click: {locateStrategy: 'xpath', selector: "(//input[@type='button'])[1]"},
                test: {locateStrategy: 'xpath', selector: "(//tr//td//select//option)[1]"},

            }
        },
        naukri_com: {
            selector: 'body',
            elements: {
                login: {locateStrategy: 'xpath', selector: "(//div[@class='mTxt'])[6]"},
                email: {locateStrategy: 'xpath', selector: "//input[@id='eLogin']"},
                password: {locateStrategy: 'xpath', selector: "//input[@id='pLogin']"},
                login_button: {locateStrategy: 'xpath', selector: "//button[@class='blueBtn']"},

                skip_part: {locateStrategy: 'xpath', selector: "(//input[@class='feedbackBtn fl'])[1]"},
                update_profile: {locateStrategy: 'xpath', selector: "//div[@class='mb10']"},
                edit_profile: {locateStrategy: 'xpath', selector: "//em[@class='icon edit']"},
                update_name: {locateStrategy: 'xpath', selector: "//input[@id='name']"},
                save_button: {locateStrategy: 'xpath', selector: "//button[@id='saveBasicDetailsBtn']"},












            }
        }

    }
}