Feature: This is a smoke scenarios for salesforce

  @docman @basic1
  Scenario: This is for verifying file is added properly and trace no is generated
    Given User AFBO-QA URL and click on document management under Administration tab
    When Click on "SaveTo DocMan" link and perform varies steps
    And  Browse the pdf document and click on next
    And  And User select any required field
    And  User enters the client code and details
    Then User clicks on submit check wheather trace no is generated or not

  @docman @basic2
  Scenario: This is for verifying file is added properly and trace no is generated
    Given User AFBO-QA URL and click on document management under Administration tab
    When Click on "SaveTo DocMan" link and perform varies steps
    And  Browse the pdf document and click on next
    And  And User select any required field
    And  User enters the client code and details
    And  User enters the client code and details and assign task
    Then User clicks on submit check wheather trace no is generated or not


  @docman @basic3
  Scenario: This is for verifying detail report and view the details
    Given User AFBO-QA URL2 and click on document management under Administration tab
    When User select audit report option and select some date and options from the dropdown for report type
    Then User click on summary report and detail report and view the details


  @docman @basic4
  Scenario: This is for verifying success result in health view
    Given User AFBO-QA URL2 and click on document management under Administration tab
    When User click on health check
    Then User see all the results are successful

  @docman @basic5
  Scenario: This is for verifying trace no in search link in document management
    Given User AFBO-QA URL and click on document management under Administration tab
    When User click on search and view link and enter trace no in trace coloum
    Then User is able to see the details of the entered trace no

  @prac @basic5
  Scenario: This is for verifying trace no in search link in document management
    Given User AFBO-QA URL and click on document management under Administration tab1
    When User login into naukri
    And User update details
    Then  user logout from application



