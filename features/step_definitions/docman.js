var data = require('../../TestResources/docman');
var Objects = require(__dirname + '/../../repository/docman.js');
var action = require(__dirname + '/../../features/step_definitions/ReusableTests.js');
var fs = require('fs');
var oldWindow,newWindow,oldWindow1,newWindow1
function initializePageObjects(browser, callback) {
    var BPPage = browser.page.docman();
    docman = BPPage.section.docmanHomepage;
    naukri = BPPage.section.naukri_com;
    callback();
}
//1
module.exports = function() {
    this.Given(/^User AFBO-QA URL and click on document management under Administration tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            URL = data.urldocs;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });

    this.When(/^Click on "SaveTo DocMan" link and perform varies steps$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@admin', data.wait);
                docman.click('@admin');
                docman.waitForElementVisible('@data_management', data.wait);
                docman.click('@data_management');
                docman.waitForElementVisible('@save_link', data.wait);
                docman.click('@save_link');

            });
        }
    })

    this.When(/^Browse the pdf document and click on next$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                browser.setValue('input[type="file"]', require('path').resolve('C:\\Users\\Amit-r-singh\\Downloads\\testing.pdf'))
                docman.waitForElementVisible('@next', data.wait);
                docman.click('@next');
            });
        }
    })

    this.When(/^And User select any required field$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@line_bussiness', data.wait);
                docman.click('@line_bussiness');
                docman.waitForElementVisible('@select_type', data.wait);
                docman.click('@select_type');
                docman.waitForElementVisible('@document_type', data.wait);
                docman.click('@document_type');
                docman.waitForElementVisible('@beneficiary', data.wait);
                docman.click('@beneficiary');
                docman.waitForElementVisible('@submit', data.wait);
                docman.click('@submit');
                docman.waitForElementVisible('@search', data.wait);
                docman.click('@search');
                browser.pause(5000);

            });
        }
    })

    this.When(/^User enters the client code and details$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                browser.windowHandles(function (result) {
                    oldWindow = result.value[0];
                    newWindow = result.value[1];
                    browser.switchWindow(newWindow);
                    docman.waitForElementVisible('@client_code', data.wait);
                    docman.click('@client_code');
                    docman.setValue('@client_code', data.client_code)
                    docman.waitForElementVisible('@submit2', data.wait);
                    docman.click('@submit2');
                    docman.waitForElementVisible('@select_test', data.wait);
                    docman.click('@select_test');
                    browser.switchWindow(oldWindow);
                    docman.waitForElementVisible('@ssn', data.wait);
                    docman.click('@ssn');
                    docman.setValue('@ssn', data.ssn_no)
                    docman.waitForElementVisible('@add', data.wait);
                    docman.click('@add');
                });
            })
        }
    })

    this.When(/^User enters the client code and details and assign task$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@next_action', data.wait);
                docman.click('@next_action');
                docman.waitForElementVisible('@Send_for_processing', data.wait);
                docman.click('@Send_for_processing');
                browser.pause(2000)
                browser.useXpath();
                docman.waitForElementVisible('@search_select', data.wait);
                docman.click('@search_select');
                browser.pause(2000)
                browser.windowHandles(function (result) {
                    oldWindow1 = result.value[0];
                    newWindow1 = result.value[1];
                    browser.switchWindow(newWindow1);
                    browser.pause(2000)
                    browser.useXpath();
                    docman.waitForElementVisible('@last_name_select', data.wait);
                    docman.click('@last_name_select');
                    docman.setValue('@last_name_select', data.last_name)
                    docman.waitForElementVisible('@kuna_search_select', data.wait);
                    docman.click('@kuna_search_select');
                    docman.waitForElementVisible('@Select_name', data.wait);
                    docman.click('@Select_name');
                    docman.waitForElementVisible('@Select_role', data.wait);
                    docman.click('@Select_role');
                    docman.waitForElementVisible('@Select_role_qa', data.wait);
                    docman.click('@Select_role_qa');
                    docman.waitForElementVisible('@assign', data.wait);
                    docman.click('@assign');
                    browser.switchWindow(oldWindow1);
                    browser.pause(2000)
                });

            })
        }
    })




    this.Then(/^User clicks on submit check wheather trace no is generated or not$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                browser.windowHandles(function (result) {

                    docman.waitForElementVisible('@main_submit', data.wait);
                    docman.click('@main_submit');
                    browser.pause(4000)
                    browser.useXpath()
                    docman.waitForElementVisible('@trace_no', data.wait);
                    docman.getText('@trace_no', function (actual) {
                        check = actual.value
                        console.log('the trace no is='+ actual.value);

                    })
                })
            })
        }
    })


    //////////////22222////////////////

    this.Given(/^User AFBO-QA URL2 and click on document management under Administration tab$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            URL = data.audit_url;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });


    this.When(/^User select audit report option and select some date and options from the dropdown for report type$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@audit_report', data.wait);
                docman.click('@audit_report');
                docman.waitForElementVisible('@report_type', data.wait);
                docman.click('@report_type')
                docman.waitForElementVisible('@all_counts', data.wait);
                docman.click('@all_counts')
                docman.waitForElementVisible('@view_summary', data.wait);
                docman.click('@view_summary')
                browser.useXpath()
                browser.pause(3000)
                docman.waitForElementVisible('@captured', data.wait);
                docman.getText('@captured',function(actual){
                    console.log('Total Captured for all counts:'+ actual.value);
                })
                docman.waitForElementVisible('@detail_report', data.wait);
                docman.click('@detail_report')
                browser.pause(3000)
                docman.waitForElementVisible('@back', data.wait);
                docman.click('@back')
            });
        }
    });

    this.Then(/^User click on summary report and detail report and view the details$/, function () {
        var URL;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            URL = data.audit_url;
            initializePageObjects(browser, function () {
                browser.pause(3000)
                docman.waitForElementVisible('@report_type', data.wait);
                docman.click('@report_type')
                docman.waitForElementVisible('@save_to', data.wait);
                docman.click('@save_to')
                docman.waitForElementVisible('@view_summary', data.wait);
                docman.click('@view_summary')
                browser.useXpath()
                docman.waitForElementVisible('@captured', data.wait);
                docman.getText('@captured',function(actual){
                    console.log('Total Captured for save to upload counts:'+ actual.value);
                })
                docman.waitForElementVisible('@detail_report', data.wait);
                docman.click('@detail_report')
                browser.pause(3000)
            });
        }
    });


    ////////////333333333333333////////

    this.When(/^User click on health check$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@Health_Check', data.wait);
                docman.click('@Health_Check');
                docman.waitForElementVisible('@success', data.wait);

            });
        }
    })
    this.Then(/^User see all the results are successful$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@success', data.wait);
            });
        }
    })

    ///////////////////4444444444/////////////////
    this.When(/^User click on search and view link and enter trace no in trace coloum$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementVisible('@admin', data.wait);
                docman.click('@admin');
                docman.waitForElementVisible('@data_management', data.wait);
                docman.click('@data_management');
                docman.waitForElementVisible('@search_view', data.wait);
                docman.click('@search_view');
                docman.waitForElementVisible('@track', data.wait);
                docman.click('@track');
                docman.setValue('@track',data.traceno);
            });
        }
    })

    this.Then(/^User is able to see the details of the entered trace no$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                docman.waitForElementPresent('@search_click', data.wait);
                docman.click('@search_click');
                browser.pause(10000)
            });
        }
    })






    this.Given(/^User AFBO-QA URL and click on document management under Administration tab1$/, function () {
        var URL5;
        browser = this;
        var execEnv = data["TestingEnvironment"];
        if (execEnv.toUpperCase() == "QA") {
            URL5 = data.urldocs5;
            initializePageObjects(browser, function () {
                browser.maximizeWindow()
                    .deleteCookies()
                    .url(URL5);
                browser.timeoutsImplicitWait(30000);
            });
        }
    });
    this.Then(/^User login into naukri$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                browser.windowHandles()
                browser.windowHandles(function (result) {
                    oldWindow = result.value[0];
                    newWindow = result.value[1];
                    browser.switchWindow(newWindow);
                    browser.closeWindow()
                    browser.switchWindow(oldWindow);

                })
                naukri.waitForElementPresent('@login', data.wait);
                naukri.click('@login');
                naukri.waitForElementPresent('@email', data.wait);
                naukri.setValue('@email', data.emailid);
                naukri.waitForElementPresent('@password', data.wait);
                naukri.setValue('@password', data.password_naukri);
                naukri.waitForElementPresent('@login_button', data.wait);
                naukri.setValue('@login_button', data.password_naukri);
                browser.pause(2000)
            })
        }
    })
    this.Then(/^User update details$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                naukri.waitForElementPresent('@update_profile', data.wait);
                naukri.click('@update_profile');
                naukri.waitForElementPresent('@edit_profile', data.wait);
                naukri.click('@edit_profile');
                naukri.waitForElementPresent('@update_name', data.wait);
                naukri.clearValue('@update_name')
                naukri.setValue('@update_name', data.updated_name);
                naukri.waitForElementPresent('@save_button', data.wait);
                naukri.click('@save_button');
            });
        }
    })
    this.Then(/^user logout from application$/, function () {
        browser = this;
        var execEnv = data["TestingEnvironment"];
        console.log('Test Environment: ' + execEnv);
        if (execEnv.toUpperCase() == "QA") {
            initializePageObjects(browser, function () {
                var xpath="(//div[@class='mTxt'])[7]";
                var xpath1="//li//a[contains(.,'Logout')]";
                browser.useXpath().moveToElement(xpath,0,1)
                browser.pause(3000);
                browser.click(xpath1);
                browser.pause(3000);
            });
        }
    })



}